# StuttgartJulia meetup
We use this repository as our main coordination point for the Julia Meetup in Stuttgart.
Julia is a young high-performance, dynamically typed programming language, which is especially powerful for numerical and scientific computing.
This group is **open for everybody**, beginners included.\
Please open a new issue if you have any questions or suggestions. There is also an accompanying [meetup.com](https://www.meetup.com/de-DE/stuttgart-julia-programming-language-meetup-gruppe/) group to reach more users, but we will keep the main information here in order to be more flexible. We hold meetings via Jitsi on a monthly basis. Please send an email to juliane.weilbach@protonmail.com if you are interested in participating.
We are collaborating with the Julia meetup in Vancouver. Check it out [Vancouver meetup](https://gitlab.com/van-julia/meetup).

# Some Recources

- Community: [Discourse](https://discourse.julialang.org/), [Gitter](https://gitter.im/JuliaLang/julia), [Zulip](https://julialang.zulipchat.com/), [YouTube](https://www.youtube.com/channel/UC9IuUwwE2xdjQUT_LMLONoA)
- Editors/IDEs/notebooks: [editor support](https://github.com/JuliaEditorSupport/),
[VSCode](https://www.julia-vscode.org/), [Pluto](https://juliahub.com/docs/Pluto/), [IJulia](https://julialang.github.io/IJulia.jl/stable/)
- Numerical libraries: [Optimization](https://www.juliaopt.org/), [Machine Learning](https://fluxml.ai/)
- Nice blog about debugging in Julia: [Debugging](https://opensourc.es/blog/basics-debugging/)

# Planned meetups

## [2021 September 11th, 7:00pm (MEZ)]

* Speaker: Christian Weilbach
* Notes: Introduction round
    * just-in-time (JIT) compiler
    * comparison to other state-of-the-art runtime designs
    * How is the Julia compiler optimizing our code? 
    * GraalVM 
    * How is a modern(ized) runtime handling some of the latency/performance issues?
    * [**Material**](/Compiler/Julia-and-JIT-compilation.pdf)
    * Interesting links mentioned in the meetup: [Tutorial on Precompilation](https://julialang.org/blog/2021/01/precompile_tutorial/) and [Profiling type-inference](https://julialang.org/blog/2021/01/snoopi_deep/)


## [2021 June 12th, 6:30pm (MEZ)]

* Speaker: Jose Storopoli
* Notes: Introduction round
   * What is Turing?
   * How to specify a model (model-macro)
   * How to specify a MCMC sampler (NUTS, HMC, MH etc.)
   * How to inspect chains and plot stuff with MCMCChains.jl
   * Better tricks to avoid for-loops inside model-macro (like lazyarrays and filldist)
   * How to use stuff that isn't available in Stan (HMM with discrete parameters being sampled by ParticleGibbs and the continuous stuff with NUTS)
   * To end we do non-centered parametrization and QR decomposition
   * [**Material**] (https://storopoli.io/Bayesian-Julia/pages/4_Turing/)



## [2021 Mai 16th, 6pm (MEZ)]

* Speaker: Simon Zollner
* Notes: Introduction round
    * Brief Introduction in Digital Signal Processing with ThinkDSP
    * He shows us how he implemented his package ThinkDSP.jl ...
    * .. and how to use it
    * additionally he shows us how to use Pluto
    * [**Material**](/DSP/Presentation_v0.1.ipynb)

## [2021 April 17th, 6pm (MEZ)]

* Speaker: Maren Hackenberg
* Notes: Introduction round
    * Brief Introduction in her publication focusing on uncovering complex structures with small datasets in a medical setting [Paper](https://arxiv.org/abs/2012.00634)
    * She shows us how to combine variational autoencoder with an ODE system and jointly learn the parameters of the model and optimize the ODE system  
    * Introduction to the DiffEqFlux.jl package
    * going through an application build with DiffEqFlux regarding to her paper
    * [**Material**](/DiffEqFlux/CombiningNNsAndDifferentialEquationsWithDiffEqFlux.jl.ipynb)


## [2021 March 12th, 6pm (MEZ)]

* Speaker: Sara Al-Rawi
* Notes: Introduction round
    * Brief introduction to RNA sequencing with Bayesian Inference for a Generative Model [Paper](https://www.biorxiv.org/content/10.1101/292037v1)
    * going through a Flux implementation of a Variational Autoencoder [Flux Tutorial](https://github.com/FluxML/model-zoo/blob/master/vision/vae_mnist/vae_mnist.jl)
    * [**Material**](/VAE/Single-cell_RNA_Sequencing_Analysis_and_Variational_Autoencoder_with_Flux_Package_.ipynb)



## [2021 February 06th, 6pm (MEZ)]

* Speaker: Juliane Weilbach
* Notes: Introduction round
    * Introduction into Ordinary Differential Equations
    * How to implement ODEs with [DifferentialEquations.jl](https://github.com/SciML/DifferentialEquations.jl)
    * Brief introduction into Gradient Descent for parameter estimation of ODEs
    * Using Python packages from Julia
    * [**Material**](/ODEs/ode.ipynb)

## Sponsors
![Lambdaforge Logo](/images/logolf.png)
